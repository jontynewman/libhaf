# libhaf

A proof of concept for an HTTP application framework written in C.

*This project is yet to be completed.*

Building and installation of the library can be achieved by executing the following commands:

```bash
make         # generate libhaf.so
make install # copy libhaf.so to /usr/lib/
```

For usage, refer to `example.c`. To build and run the example, execute the previously stated commands before the following:

```bash
make example      # generate the binary
chmod u+x example # ensure that the binary is executable
./example         # run the example
```

