COMPILE=gcc -Wall -pedantic -ansi -I.

SOURCES=\
haf/application.c \
haf/request/pattern/adapter.c \
haf/request/response/adapter.c \
haf/request/pattern/response/adapter.c
OBJECTS=$(SOURCES:.c=.o)

NAME=haf
LIBRARY=lib$(NAME).so
ARCHIVE=$(LIBRARY:.so=.a)

EXECUTABLE=example

all: $(SOURCES) $(LIBRARY)

$(LIBRARY): $(ARCHIVE)
	$(COMPILE) -shared $(OBJECTS) -o $@

$(ARCHIVE): $(OBJECTS)
	ar rc $@ $(OBJECTS)
	ranlib $@

.c.o:
	$(COMPILE) -fPIC -c $< -o $@

$(EXECUTABLE):
	$(COMPILE) -L. -l$(NAME) $@.c -o $@

install:
	cp $(LIBRARY) /usr/lib/

clean:
	rm -f $(EXECUTABLE) $(LIBRARY) $(ARCHIVE) $(OBJECTS)
