#ifndef HAF_APPLICATION_H
#define HAF_APPLICATION_H

#include <haf/request/handler.h>
#include <haf/request/pattern/handler.h>
#include <haf/request/response/handler.h>
#include <haf/request/pattern/response/handler.h>

typedef struct haf_application haf_application_t;

struct haf_application *haf_application_init();

void haf_application_push(
	struct haf_application *application,
	haf_request_handler_t handler
);

void haf_application_push_pattern(
	struct haf_application *application,
	const char* pattern,
	haf_request_pattern_handler_t handler
);

void haf_application_push_response(
	struct haf_application *application,
	haf_request_response_handler_t handler
);

void haf_application_push_pattern_response(
	struct haf_application *application,
	const char* pattern,
	haf_request_pattern_response_handler_t handler
);

bool haf_application_run(
	struct haf_application *application,
	struct haf_request *request
);

#endif
