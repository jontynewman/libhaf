#include <haf/application.h>
#include <haf/request.h>
#include <haf/request/handler.h>
#include <haf/request/handler/type.h>
#include <haf/request/pattern/handler.h>
#include <haf/request/pattern/adapter.h>
#include <haf/request/response/handler.h>
#include <haf/request/response/adapter.h>
#include <haf/request/pattern/response/handler.h>
#include <haf/request/pattern/response/adapter.h>
#include <stddef.h>
#include <stdlib.h>

typedef struct haf_application_request_handler {
	union {
		haf_request_handler_t request;
		haf_request_pattern_handler_t request_pattern;
		haf_request_response_handler_t request_response;
		haf_request_pattern_response_handler_t request_pattern_response;
	} handler;
	haf_request_handler_type_t type;
	const char *pattern;
	struct haf_application_request_handler *next;
} haf_application_request_handler_t;

struct haf_application {
	haf_application_request_handler_t *first;
	haf_application_request_handler_t *last;
};

haf_application_t *haf_application_init() {

	haf_application_t *app;
	app = (haf_application_t *) malloc(sizeof(haf_application_t));

	app->first = NULL;
	app->last = app->first;

	return app;
}

haf_application_request_handler_t *haf_application_request_handler_init() {
	static const size_t size = sizeof(haf_application_request_handler_t);
	return (haf_application_request_handler_t *) malloc(size);
}

void haf_application_push_handler(
	haf_application_t *application,
	haf_application_request_handler_t *handler
) {

	handler->next = NULL;

	if (NULL == application->first) {
		application->first = handler;
	}

	if (NULL == application->last) {
		application->last = application->first;
	} else {
		application->last->next = handler;
		application->last = handler;
	}
}

void haf_application_push(
	haf_application_t *application,
	haf_request_handler_t handler
) {

	haf_application_request_handler_t *converted;
	converted = haf_application_request_handler_init();
	converted->handler.request = handler;
	converted->type = HAF_REQUEST_HANDLER_TYPE_REQUEST;

	haf_application_push_handler(
		application,
		converted
	);
}

void haf_application_push_pattern(
	haf_application_t *application,
	const char *pattern,
	haf_request_pattern_handler_t handler
) {
	haf_application_request_handler_t *converted;
	converted = haf_application_request_handler_init();
	converted->handler.request_pattern = handler;
	converted->pattern = pattern;
	converted->type = HAF_REQUEST_HANDLER_TYPE_REQUEST_PATTERN;

	haf_application_push_handler(application, converted);
}

void haf_application_push_response(
	haf_application_t *application,
	haf_request_response_handler_t handler
) {
	haf_application_request_handler_t *converted;
	converted = haf_application_request_handler_init();
	converted->handler.request_response = handler;
	converted->type = HAF_REQUEST_HANDLER_TYPE_REQUEST_RESPONSE;

	haf_application_push_handler(application, converted);
}

void haf_application_push_pattern_response(
	haf_application_t *application,
	const char *pattern,
	haf_request_pattern_response_handler_t handler
) {
	haf_application_request_handler_t *converted;
	converted = haf_application_request_handler_init();
	converted->handler.request_pattern_response = handler;
	converted->pattern = pattern;
	converted->type = HAF_REQUEST_HANDLER_TYPE_REQUEST_PATTERN_RESPONSE;

	haf_application_push_handler(application, converted);
}

bool haf_application_run(
	haf_application_t *application,
	haf_request_t* request
) {

	bool handled = false;
	haf_application_request_handler_t *current = application->first;

	while (!handled && NULL != current) {

		switch (current->type) {
			case HAF_REQUEST_HANDLER_TYPE_REQUEST:
				handled = current->handler.request(request);
				break;
			case HAF_REQUEST_HANDLER_TYPE_REQUEST_PATTERN:
				handled = haf_request_pattern_adapter(
					request,
					current->handler.request_pattern
				);
				break;
			case HAF_REQUEST_HANDLER_TYPE_REQUEST_RESPONSE:
				handled = haf_request_response_adapter(
					request,
					current->handler.request_response
				);
				break;
			case HAF_REQUEST_HANDLER_TYPE_REQUEST_PATTERN_RESPONSE:
				handled = haf_request_pattern_response_adapter(
					request,
					current->handler.request_pattern_response
				);
				break;
		}

		current = current->next;
	}

	return handled;
}
