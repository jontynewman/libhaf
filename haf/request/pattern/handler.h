#ifndef HAF_REQUEST_PATTERN_HANDLER_H
#define HAF_REQUEST_PATTERN_HANDLER_H

#include <stdbool.h>
#include <stddef.h>

struct haf_request;

typedef bool  *(*haf_request_pattern_handler_t)(
	struct haf_request *request,
	const char **args,
	size_t length
);

#endif
