#ifndef HAF_REQUEST_PATTERN_ADAPTER_H
#define HAF_REQUEST_PATTERN_ADAPTER_H

#include <haf/request/pattern/handler.h>
#include <stdbool.h>

struct haf_request;
struct haf_request_response;

bool haf_request_pattern_adapter(
	struct haf_request *request,
	haf_request_pattern_handler_t handler
);

#endif
