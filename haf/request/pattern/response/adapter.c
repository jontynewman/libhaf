#include <haf/request.h>
#include <haf/request/pattern/response/adapter.h>
#include <stddef.h>

struct haf_request;
struct haf_request_response;

bool haf_request_pattern_response_adapter(
	haf_request_t *request,
	haf_request_pattern_response_handler_t handler
) {
	return handler(request, NULL, 0) != NULL;
}
