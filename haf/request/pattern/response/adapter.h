#ifndef HAF_REQUEST_PATTERN_RESPONSE_ADAPTER_H
#define HAF_REQUEST_PATTERN_RESPONSE_ADAPTER_H

#include <haf/request/pattern/response/handler.h>
#include <stdbool.h>

struct haf_request;
struct haf_request_response;

bool haf_request_pattern_response_adapter(
	struct haf_request *request,
	haf_request_pattern_response_handler_t handler
);

#endif
