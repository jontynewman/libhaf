#ifndef HAF_REQUEST_PATTERN_RESPONSE_HANDLER_H
#define HAF_REQUEST_PATTERN_RESPONSE_HANDLER_H

#include <stddef.h>

struct haf_request;
struct haf_request_response;

typedef struct haf_request_response *(*haf_request_pattern_response_handler_t)(
	struct haf_request *request,
	const char **args,
	size_t length
);

#endif
