#ifndef HAF_REQUEST_HANDLER_H
#define HAF_REQUEST_HANDLER_H

#include <stdbool.h>

struct haf_request;

typedef bool (*haf_request_handler_t)(
	struct haf_request *request
);

#endif
