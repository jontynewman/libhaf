#include <haf/request.h>
#include <haf/request/response/adapter.h>
#include <stddef.h>

struct haf_request;
struct haf_request_response;

bool haf_request_response_adapter(
	struct haf_request *request,
	haf_request_response_handler_t handler
) {
	return handler(request) != NULL;
}
