#ifndef HAF_REQUEST_RESPONSE_HANDLER_H
#define HAF_REQUEST_RESPONSE_HANDLER_H

struct haf_request;
struct haf_request_response;

typedef struct haf_response *(*haf_request_response_handler_t)(
    struct haf_request *request
);

#endif
