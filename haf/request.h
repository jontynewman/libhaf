#ifndef HAF_REQUEST_H
#define HAF_REQUEST_H

typedef struct haf_request haf_request_t;

haf_request_t *haf_request_init(
	const char *method,
	const char *url,
	const char **headers,
	const char **query,
	const char **data
);

const char *haf_request_method(haf_request_t *request);

const char *haf_request_url(haf_request_t *request);

const char **haf_request_headers(haf_request_t *request);

const char **haf_request_query(haf_request_t *request);

const char **haf_request_data(haf_request_t *request);

#endif
