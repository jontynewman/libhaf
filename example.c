#include <haf/application.h>
#include <haf/request.h>
#include <haf/request/response.h>
#include <stddef.h>
#include <stdio.h>

haf_request_response_t *example(
	haf_request_t *request,
	const char **args,
	size_t size
) {

	printf("Hello world!\n");
	return NULL;
}

int main(int argc, char **argv)
{
	haf_application_t *app = haf_application_init();
	haf_application_push_pattern_response(app, "/", &example);
	haf_application_run(app, NULL);

	return 0;
}
